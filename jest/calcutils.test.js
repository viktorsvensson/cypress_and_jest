const { add } = require("../calcutils.js");

describe("add", () => {

    test("returns the sum of the parameters", () => {
        const result = add(5, 2)

        expect(result).toBe(7)
    })

    test.each([
        [1, "1"],
        [1, undefined],
        [1, true],
        [1, null],
        [1, ""]
    ])("returns NaN for non-numeric parameters - %p, %p", (a, b) => {

        const result = add(a, b);
        expect(result).toBeNaN()
    })



})