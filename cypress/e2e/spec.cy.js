/// <reference types="cypress" />


beforeEach(() => {
  cy.visit("/")
})

describe('addition', () => {

  it('should allow a user to add two numbers', () => {
    
    cy.get('[data-testid="first-value"]').type("329")
    cy.get('[data-testid="second-value"]').type("329")

    cy.get('[data-testid="calc-btn"]').click()
    
    cy.get('[data-testid="result-display"]')
      .contains('contain.text', "658")

  })

  it('Should display "Ogiltig input" om ej siffror matas in', () => {
    
    cy.get('[data-testid="first-value"]').type("a")
    cy.get('[data-testid="second-value"]').type("b")

    cy.get('[data-testid="calc-btn"]').click()
    
    cy.get('[data-testid="result-display"]')
      .contains('contain.text', "Ogiltig input")

  })

})